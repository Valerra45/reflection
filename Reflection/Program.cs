﻿using Newtonsoft.Json;
using Serializer;
using System;
using System.Diagnostics;

namespace Reflection
{
    class Program
    {
        class F
        {
            public int i3;
            public int I5 { get; set; }
            public string s1 = "1";

            public bool b;
            public decimal d;
            public float f;
            public double o;

            public string Get()
            {
                return $"b = {b}; f = {f}; i3 = {i3}; o = {o}; I5 = {I5}; s1 = {s1}";
            }

            public override bool Equals(object obj)
            {
                if ((obj == null) || !this.GetType().Equals(obj.GetType()))
                {
                    return false;
                }
                else
                {
                    F f = (F)obj;

                    return (i3 == f.i3)
                        && (I5 == f.I5)
                        && (s1 == f.s1)
                        && (b == f.b)
                        && (d == f.d)
                        && (o == f.o);
                }
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        static void Main(string[] args)
        {
            const int count = 1000000;
            string csvString = null;
            string jsonString = null;

            Stopwatch stopWatch = new Stopwatch();

            F f = new F
            {
                i3 = 3,
                I5 = 5,
                b = true,
                d = 99.00m,
                f = 3.09f,
                o = 133.56
            };

            stopWatch.Start();

            for (int i = 0; i < count; i++)
            {
                csvString = CsvSerializer.SerializeFromObjectToCSV(f);
            }

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                   ts.Hours, ts.Minutes, ts.Seconds,
                   ts.Milliseconds);

            stopWatch.Reset();
            stopWatch.Start();

            Console.WriteLine($"Serialized cvs string: {csvString}");
            Console.WriteLine($"Iterations: {count}, time spent iterating: {elapsedTime}");

            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                  ts.Hours, ts.Minutes, ts.Seconds,
                  ts.Milliseconds);

            Console.WriteLine();

            Console.WriteLine($"Time taken to output to the console: {elapsedTime}");

            Console.WriteLine();

            stopWatch.Reset();
            stopWatch.Start();

            for (int i = 0; i < count; i++)
            {
                jsonString = JsonConvert.SerializeObject(f);
            }

            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                  ts.Hours, ts.Minutes, ts.Seconds,
                  ts.Milliseconds);

            Console.WriteLine($"Serialized json string: {jsonString}");
            Console.WriteLine($"Iterations: {count}, time spent iterating: {elapsedTime}");
            Console.WriteLine();

            Console.WriteLine("*********************************************************************************");

            F csvOnject = null;

            stopWatch.Reset();
            stopWatch.Start();

            for (int i = 0; i < count; i++)
            {
                csvOnject = CsvSerializer.DeserializeFromCSVToObject<F>(csvString);
            }

            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                  ts.Hours, ts.Minutes, ts.Seconds,
                  ts.Milliseconds);

            Console.WriteLine($"Deserialized from cvs string: {csvOnject.Get()}");
            Console.WriteLine($"Iterations: {count}, time spent iterating: {elapsedTime}");

            Console.WriteLine();

            F jsonOnject = null;

            stopWatch.Reset();
            stopWatch.Start();

            for (int i = 0; i < count; i++)
            {
                jsonOnject = JsonConvert.DeserializeObject<F>(jsonString);
            }

            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                  ts.Hours, ts.Minutes, ts.Seconds,
                  ts.Milliseconds);

            Console.WriteLine($"Deserialized from json string: {jsonOnject.Get()}");
            Console.WriteLine($"Iterations: {count}, time spent iterating: {elapsedTime}");

            Console.WriteLine();

            Console.WriteLine($"Result f == csvOnject: {f.Equals(csvOnject)}");
            Console.WriteLine($"Result f == jsonOnject: {f.Equals(jsonOnject)}");
        }
    }
}
