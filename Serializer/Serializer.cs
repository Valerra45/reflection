﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Serializer
{
    public static class CsvSerializer
    {
        const char seporator = ';';

        public static T DeserializeFromCSVToObject<T>(string csv)
        {
            var obj = Activator.CreateInstance<T>();

            var memberArray = obj
                .GetType()
                .GetMembers()
                .Where(x => x.MemberType == MemberTypes.Field
                || x.MemberType == MemberTypes.Property)
                .ToArray();

            var serializeArray = csv.Split(seporator);

            foreach (var item in serializeArray)
            {
                ParseMember(item, memberArray, obj);
            }

            return obj;
        }

        public static string SerializeFromObjectToCSV(object obj)
        {
            var serializeArray = obj
                .GetType()
                .GetMembers()
                .Where(x => x.MemberType == MemberTypes.Field
                || x.MemberType == MemberTypes.Property)
                .Select(x => $"{x.Name}:" +
                $"{(x.MemberType == MemberTypes.Field ? ((FieldInfo)x).GetValue(obj) : ((PropertyInfo)x).GetValue(obj))}")
                .ToArray();

            return string.Join(seporator, serializeArray);
        }

        private static void ParseMember(string str, MemberInfo[] memberArray, object obj)
        {
            var serializeData = str.Split(":");

            var member = memberArray
                .FirstOrDefault(x => x.Name.Equals(serializeData[0]));

            if (member is null)
            {
                throw new Exception("Not equivalent types");
            }

            Type type = member.MemberType == MemberTypes.Field 
                ? ((FieldInfo)member).FieldType 
                : ((PropertyInfo)member).PropertyType;

            if (type == typeof(int))
            {
                SetMemberValue<int>(int.Parse(serializeData[1]), member, obj);
            }
            else if (type == typeof(double))
            {
                SetMemberValue<double>(double.Parse(serializeData[1]), member, obj);
            }
            else if (type == typeof(decimal))
            {
                SetMemberValue<decimal>(decimal.Parse(serializeData[1]), member, obj);
            }
            else if (type == typeof(float))
            {
                SetMemberValue<float>(float.Parse(serializeData[1]), member, obj);
            }
            else if (type == typeof(string))
            {
                SetMemberValue<string>(serializeData[1], member, obj);
            }
            else if (type == typeof(bool))
            {
                SetMemberValue<bool>(serializeData[1].Equals("True"), member, obj);
            }
            else
            {
                throw new Exception("Type not supported");
            }
        }

        private static void SetMemberValue<T>(T value, MemberInfo member, object obj)
        {
            if (member.MemberType == MemberTypes.Field)
            {
                ((FieldInfo)member).SetValue(obj, value);
            }
            else
            {
                ((PropertyInfo)member).SetValue(obj, value);
            }
        }
    }
}
